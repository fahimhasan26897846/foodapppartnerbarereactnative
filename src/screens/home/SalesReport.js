import React, { Component } from 'react'
import { View, Text,StyleSheet } from 'react-native'
import { connect } from 'react-redux'

export class SalesReport extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize:16,fontWeight:'bold',alignSelf:'center',margin:20}}> There is no sale </Text>
                <View style={{height:2,width:'80%',alignSelf:'center',backgroundColor:'#646464'}}></View>
                <View style={{position:'absolute',bottom:0,left:0,width:'100%',height:60,borderTopWidth:2,borderTopColor:'#000',flexDirection:'row',justifyContent: 'space-between',paddingLeft:20,paddingRight:20,padding:5}}>
                    <Text style={{fontSize:15,fontWeight:'bold',color:'#646464'}}>Total Sales</Text>
                    <Text style={{fontSize:15,fontWeight:'bold',color:'#646464'}}>£
 0</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff'
    }
})

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(SalesReport)
