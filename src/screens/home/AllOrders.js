import React, { Component } from 'react'
import { View, Text,StyleSheet,FlatList,TouchableOpacity,ToastAndroid } from 'react-native'
import {Card,CardItem,Body,Icon,Badge} from "native-base"
import { connect } from 'react-redux'

export class AllOrders extends Component {
    state= {
        orders:[]
    }

    removeOrder = (id)=>{
        ToastAndroid.show(id.toString(),ToastAndroid.SHORT,ToastAndroid.CENTER);
        fetch(this.props.uri+'order/remove/'+id,{
                method: 'DELETE',
        }).then(()=>{
            fetch(this.props.uri+'order/fetch-by-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
                
                var newArray = responseJson.filter((item)=>{
                    return item.orderStatus == "recieved";         
                })
                this.setState({orders:newArray});
                })
        })
    }

    fetchTimeDate=(timestamp)=>{
        var iteim = new Date(timestamp).getDate();
        return iteim;
      }
    
      fetchTimeMonth=(timestamp)=>{
        var iteim = new Date(timestamp).getMonth();
        return iteim;
      }
    
      fetchTimeYear=(timestamp)=>{
        var iteim = new Date(timestamp).getYear();
        return iteim;
      }

      
    componentDidMount = async()=>{

        fetch(this.props.uri+'order/fetch-by-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
        console.log(responseJson);
        var newArray = responseJson.filter((item)=>{
            return item.orderStatus == "recieved";         
        })
        this.setState({orders:newArray});
        })
    }
    render() {
        return (
            <View style={styles.container}>
              {this.state.orders.length<1?<Card>
                  <CardItem>
                      <Body>
                          <Text style={{color:'#646464',fontSize:15}}>There is no order request</Text>
                      </Body>
                  </CardItem>
              </Card>:<FlatList
        data={this.state.orders}
        renderItem={({item})=><TouchableOpacity onPress={()=>{this.props.navigation.navigate('Order',{orderId:item._id})}}><Card>
        <CardItem>
            <Body>
                <Icon onPress={()=>{this.removeOrder(item._id)}} type="Ionicons" name="trash-outline" style={{fontSize:24,color:'#d2181b',alignSelf:'flex-end',}} />
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>{item.orderNumber}</Text>
                    <Badge style={{ backgroundColor: '#646464' }}>
  <Text style={{ color: 'white',textTransform:'capitalize' }}>{item.orderStatus}</Text>
</Badge>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>Order Date</Text>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>{this.fetchTimeDate(item.createdAt)+'/'+this.fetchTimeMonth(item.createdAt)+'/'+this.fetchTimeYear(item.createdAt)}</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>Delivery Time</Text>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>{item.deliveryTime}</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>Payment Method</Text>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>{item.paymentMethod}</Text>
                </View>
                <View style={{width:'100%',height:2,backgroundColor:'#646464',marginTop:15,marginBottom:15}}></View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>Total Price</Text>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>£{item.totalPrice}</Text>
                </View>
            </Body>
        </CardItem>
    </Card></TouchableOpacity>}
        keyExtractor={item => item.id}
      />}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
      userId: state.auth.userId,
      uri:state.auth.uri
    }
  };

const styles = StyleSheet.create({
    container:{
        flex:1,
    }
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(AllOrders)
