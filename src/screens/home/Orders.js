import React, { Component } from 'react'
import { View, Text,FlatList,StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import AllOrders from "./AllOrders"
import PendinOrders from "./PendingOrders"
import {Container,Header,Tabs,Tab} from "native-base"
export class Orders extends Component {

    render() {
        return (
            <View style={styles.container}>
              <Tabs tabBarUnderlineStyle={{backgroundColor:'#d2181b'}}>
                  <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}} tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white", }} heading="All Orders">
                      <AllOrders navigation={this.props.navigation} />
                  </Tab>
                  <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}}  tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white" }} heading="Pending Orders">
                      <PendinOrders navigation={this.props.navigation} />
                  </Tab>
              </Tabs>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff'
    }
})

const mapStateToProps = state => {
    return {
      userId: state.auth.userId,
    }
  };

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Orders)
