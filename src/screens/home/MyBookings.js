import React, { Component } from 'react'
import { View, Text,StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import {Container,Header,Tabs,Tab} from "native-base"

export class MyBookings extends Component {
 

    render() {
        return (
            <View style={{flex:1}}>
                 <Tabs tabBarUnderlineStyle={{backgroundColor:'#d2181b'}}>
                  <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}} tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white", }} heading="ALL BOOKINGS">
                      <AllBookings navigation={this.props.navigation} />
                  </Tab>
                  <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}}  tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white" }} heading="PENDING BOOKINGS">
                      <PendingBookings navigation={this.props.navigation} />
                  </Tab>
                </Tabs>
            </View>
        )
    }
}

export class AllBookings extends Component {
 

    render() {
        return (
            <View style={styles.container}>
                <View style={{width:'100%',padding:10,backgroundColor:'white',alignItems:'center'}}>
                <Text>There is no bookings</Text>
                </View>
            </View>
        )
    }
}

export class PendingBookings extends Component {
 

    render() {
        return (
            <View style={styles.container}>
                <View style={{width:'100%',padding:10,backgroundColor:'white',alignItems:'center'}}>
                <Text>There is no pending bookings</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#e4e4e4'
    }
})

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(MyBookings)
