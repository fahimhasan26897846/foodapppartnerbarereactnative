import React, { Component } from 'react'
import { View, Text ,StatusBar,StyleSheet,TouchableOpacity,Image} from 'react-native'
import { connect } from 'react-redux'
import {Icon} from "native-base"

export class Home extends Component {

    state = {
        imgUri:'',
        time:'',
        title:'',
        speciality:'',
        location:'',
        openingTime:'',
        closingTime:''
    }
 
    componentDidMount = () => {
        fetch(this.props.uri+'admin/fetch-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
            console.log(responseJson.adminUser.restaurants);
            this.setState({title:responseJson.adminUser.restaurants[0].name,imgUri:this.props.baseUri+responseJson.adminUser.restaurants[0].logo,speciality:responseJson.adminUser.restaurants[0].speciality.toString(),location:responseJson.adminUser.restaurants[0].address+','+responseJson.adminUser.restaurants[0].postCode,openingTime:responseJson.adminUser.restaurants[0].openingTime,closingTime:responseJson.adminUser.restaurants[0].closingTime});
        });
            
    }
    render() {
        let photo = '';
        if(this.state.imgUri === ''){
            photo = require('../../../assets/resturent.png');
        }else{
            photo = {uri:this.state.imgUri}
        }
        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor="black" />
                <View style={{height:10,position:'absolute',top:0,left:0,width:'100%',backgroundColor:'#d2181b'}}></View>
                <Image source={photo} style={{height:150,width:150,backgroundColor:'black',borderRadius:100,marginTop:100}} />
                <Text style={{alignItems:'center',fontSize:25,letterSpacing:2,margin:20,fontWeight:'bold'}}>{this.state.title}</Text>
                <Text style={{alignItems:'center',fontSize:18,letterSpacing:2}}>{this.state.speciality}</Text>
                <Text style={{alignItems:'center',fontSize:16,fontWeight:'bold'}}>{this.state.location}</Text>
                <View style={{padding:5,paddingLeft:30,paddingRight:30,alignItems:'center',justifyContent:'center',backgroundColor:'#fbc02d',borderWidth:2,borderColor:'orange',borderRadius:20,margin:20}}><Text style={{letterSpacing:2}}>{this.state.openingTime}-{this.state.closingTime}</Text></View>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Driver')}} style={{backgroundColor:'#424242',padding:15,flexDirection:'row',width:140,alignItems:'center',justifyContent:'center',marginRight:10}}>
                    <Icon type="Feather" name="search" style={{fontSize:20,color:'white'}}/>
                        <Text  style={{color:'#fff',marginLeft:10}}>Driver</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('HouseOrder')}} style={{backgroundColor:'#d2181b',padding:15,width:130,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color:'#fff',fontWeight:'bold'}}>HOUSE ORDER</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        justifyContent:'center'
    }
})

const mapStateToProps = state => {
    return {
      userId: state.auth.userId,
      uri:state.auth.uri,
      baseUri:state.auth.baseUri
    }
  };

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
