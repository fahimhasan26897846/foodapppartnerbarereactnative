import React, { Component } from 'react'
import { View, Text, StatusBar,StyleSheet, TouchableOpacity,Image,ScrollView,Dimensions } from 'react-native'
import { connect } from 'react-redux'

const {height,width} = Dimensions.get('window');
export class Settings extends Component {
   
    render() {
        return (
            <View style={styles}>
                <StatusBar barStyle="light-content" backgroundColor="black" />
                <View style={{height:10,position:'absolute',top:0,left:0,width:'100%',backgroundColor:'#d2181b'}}>

                </View>
                <View style={{height:60,position:'absolute',top:10,elevation:10,left:0,width:'100%',backgroundColor:'#ffffff'}}>
                <Text style={{fontSize:20,fontWeight:'bold',marginTop:10,marginLeft:10}}>Settings</Text>
                </View>
                <ScrollView style={{marginTop:70,paddingTop:20,zIndex:20,height:600,backgroundColor:'#ffffff'}}>
                    
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('BusinessProfile')}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15,height:70}}>
                        <Image source={require('../../../assets/img/businessprofile.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >Business Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Orders')}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15,height:70}}>
                        <Image source={require('../../../assets/img/myorders.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >My orders</Text>
                    </TouchableOpacity>
                   
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('MyBookings')}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15,height:70}}>
                        <Image source={require('../../../assets/img/mybokings.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >My Bookings</Text>
                    </TouchableOpacity>
                   
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('SalesReport')}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15,height:70}}>
                        <Image source={require('../../../assets/img/salesreport.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >Sales Report</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Notifications')}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15,height:70}}>
                        <Image source={require('../../../assets/img/notifications.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >Notifications</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Termsofuse')}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15,height:70}}>
                        <Image source={require('../../../assets/img/termsofuse.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >Terms of use</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Printer')}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15}}>
                        <Image source={require('../../../assets/img/printer.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >Printer</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.logout(false)}} style={{flexDirection:'row',alignItems:'center',width:'100%',paddingLeft:15,height:70,marginBottom:120}}>
                        <Image source={require('../../../assets/img/signout.png')} style={{height:60,width:60}} />
                        <Text style={{fontSize:20,marginLeft:20,fontWeight:'bold',color:'#424242',marginTop:-5}} >Sign Out</Text>
                    </TouchableOpacity>
                   


                </ScrollView>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#ffffff',
    }
})

const mapStateToProps = state => {
    return {
      host: state.auth.host,
    }
  };

const mapDispatchToProps = dispatch => {
    return{
        logout:(value)=>{dispatch({type:'LOGOUT',info:value})}
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
