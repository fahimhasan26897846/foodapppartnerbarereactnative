import React, { Component } from 'react'
import { View, Text,StatusBar,StyleSheet,Dimensions } from 'react-native'

import { connect } from 'react-redux'

export class Termsofuse extends Component {
    

    render() {
        return (
            <View style={styles.container}>
                <Text> No terms of use available </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{flex:1,padding:10}
})

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Termsofuse)
