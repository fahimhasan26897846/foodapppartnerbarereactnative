import React, { Component } from 'react'
import { View, Text,StyleSheet,ScrollView,Linking,ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import {BLEPrinter} from "react-native-thermal-receipt-printer";
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import {Header,Container,Left,Right,Body,Icon,Title,Button} from "native-base"
import Timeline from 'react-native-timeline-flatlist'
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component'

export class Order extends Component {
    state = {
        resturentName:'',
        resturentLocation:'',
        orderNumber:'',
        name:'',
        userAddress:'',
        userPhone:'',
        orderDate:'',
        totalPrice:'',
        printing:true,
        printers:[],
        currentPrinter:'',
        foodItems:[],
        paymentMethod:'',
        deliveryCharge:'',
        discountCharge:'',
        discount:'',
       data:[ {time: '0', title: 'Received',lineColor:'#d2181b',circleColor:'#d2181b'},
      {time: '0', title: 'Picked up',lineColor:'#adadad',circleColor:'#d2181b'},
      {time: '0', title: 'Delivered',lineColor:'#adadad',circleColor:'#adadad'}],
      tableHead: ['Qty', 'Description', 'Amount'],
      tableData: []
    }

    fetchTimeDate=(timestamp)=>{
        var iteim = new Date(timestamp).getDate();
        return iteim;
      }
    
      fetchTimeMonth=(timestamp)=>{
        var iteim = new Date(timestamp).getMonth();
        return iteim;
      }
    
      fetchTimeYear=(timestamp)=>{
        var iteim = new Date(timestamp).getYear();
        return iteim;
      }

    componentDidMount = async() => 
    {
        fetch(this.props.uri+'order/fetch/'+this.props.route.params.orderId).then((response)=>response.json()).then((responseJson)=>{
            console.log(responseJson);
            var newArray = [];
        for(var i=0;i<responseJson.data.foodItems.length;i++){
                var modifiedArray = [responseJson.data.foodItems[i].quantity,responseJson.data.foodItems[i].title,'£ '+parseInt(responseJson.data.foodItems[i].price)*parseInt(responseJson.data.foodItems[i].quantity)];
            newArray.push(modifiedArray);}
            this.setState({resturentName:responseJson.data.restaurant.name,orderNumber:responseJson.data.orderNumber,resturentLocation:responseJson.data.address,orderDate:responseJson.data.createdAt,totalPrice:responseJson.data.totalPrice,paymentMethod:responseJson.data.paymentMethod,deliveryCharge:responseJson.data.deliveryCharge,discount:responseJson.data.discount,discountCharge:responseJson.data.restaurant.deliveryCharges,name:responseJson.data.user.name,userAddress:responseJson.data.user.address+','+responseJson.data.user.postCode,userPhone:responseJson.data.user.mobile,tableData:newArray,foodItems:responseJson.data.foodItems})
            });         
    }
    printBillTest = () => {
        var dates = new Date(this.state.orderDate);
        this.state.currentPrinter && BLEPrinter.printBill("<CM>"+this.state.resturentName+"</CM>\n<C>Address:"+this.state.resturentLocation+"</C>\n<C>Date:"+dates.getDate().toString()+"/"+dates.getMonth().toString()+"/"+dates.getFullYear().toString()+"</C>\n\n<C>Order no:"+this.state.orderNumber+"</C>\n<C>"+"QTY"+" "+" "+" "+" "+"Description"+" "+" "+" "+" "+"Amount</C>\n____________________________\n\n"+this.state.foodItems.map(arr=>"<C>"+arr.quantity.toString()+" "+" "+" "+" "+arr.title.toString()+" "+" "+" "+" "+" "+arr.price.toString()+"</C>\n")+"\n\n____________________________\n\n<C>GROSS TOTAL:"+" "+" "+" "+" "+" "+" "+this.state.totalPrice+"</C>\n<C>DISCOUNT CHARGES:"+" "+" "+" "+"" /+" "+" "+this.state.deliveryCharge+"  </C>\n<C>DELIVERY FEES:"+" "+" "+" "+" "+" "+this.state.deliveryCharge+"  </C>\n<C>SAVINGS</C>\n________________________________\n<L>  TOTAL PAYMENT (CASH/CARD)  </L><R>  "+this.state.totalPrice.toString()+"  </R>\n\n\n\n\n\n<C>DELIVERY TO: "+this.state.userName+", "+this.state.userAddress+"</C>\n\n<C>OR,</C>\n\n\n<C>COLLECTION FOR CUSTOMER NAME</C>");
      }

    _connectPrinter =(printer) => {
        BLEPrinter.connectPrinter(printer).then(value=>{
          this.setState({currentPrinter:value});
        })
        .then(()=>{
          this.printBillTest();
        }).then(()=>{
          this.setState({printing:false});
          ToastAndroid.show('Printing completed',ToastAndroid.CENTER,ToastAndroid.SHORT);
        })
      }

    printContent = async()=>{
        await BluetoothStateManager.enable().then((result) => {
             BLEPrinter.init().then(()=> {
                BLEPrinter.getDeviceList().then(value=>{
                  if(this.props.activeBluetoothPrinter === ''){
                    ToastAndroid.show('Set your printer first',ToastAndroid.CENTER,ToastAndroid.SHORT);
                   
                  }else{
                     this._connectPrinter(this.props.activeBluetoothPrinter);
                  }
                  
                });
              });
        });
      
    }

    renderDetail(rowData, sectionID, rowID) {
        let title = <Text style={[styles.title]}>{rowData.title}</Text>
        var desc = null
        if(rowData.description && rowData.imageUrl)
          desc = (
            <View style={styles.descriptionContainer}>   
              <Image source={{uri: rowData.imageUrl}} style={styles.image}/>
              <Text style={[styles.textDescription]}>{rowData.description}</Text>
            </View>
          )
           
    return (
        <View style={{flex:1}}>
          {title}
          {desc}
        </View>
      )
    }

    render() {
        return (
            <View style={{flex:1,backgroundColor:'#fff'}}>
            <Header style={{ backgroundColor: 'white',}}
          androidStatusBarColor="white">
              <Left>
                <Button onPress={()=>{this.props.navigation.goBack()}} transparent>
                  <Icon style={{color:'black'}} name='arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title style={{color:'black'}}>Order Details</Title>
              </Body>
              <Right>
                <Button onPress={()=>{this.printContent()}} style={{backgroundColor:'#d2181b',height:30,paddingLeft:15,paddingRight:15}}>
                  <Text style={{fontSize:10,color:'white'}}>Reprint</Text>
                </Button>
              </Right>
            </Header>
             <ScrollView style={{flex:1,marginBottom:20}}>
             <Timeline
         data={this.state.data}
         style={{marginLeft:70,marginRight:70,marginTop:50}}
          columnFormat='single-column-right'
        />
             <View style={{width:'100%',height:1,backgroundColor:'#000',marginTop:20}}></View>
        <View style={{width:'100%',paddingTop:10}}>
            <Text style={{fontSize:14,fontWeight:'bold',alignSelf:'center'}}>{this.state.resturentName}</Text>
            <Text style={{fontSize:14,alignSelf:'center'}}>{this.state.resturentLocation}</Text>
            <Text style={{fontSize:14,alignSelf:'center'}}>{this.fetchTimeDate(this.state.orderDate)+'/'+this.fetchTimeMonth(this.state.orderDate)+'/'+this.fetchTimeYear(this.state.orderDate)}</Text>
            <Text style={{fontSize:14,alignSelf:'center'}}>{this.state.orderNumber}</Text>
        </View>
       
        <Table borderStyle={{borderWidth: 2, borderColor: '#fff'}}>
          <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text}/>
          <Rows data={this.state.tableData} textStyle={styles.text}/>
        </Table>
        <View style={{width:'100%',height:1,backgroundColor:'#000',marginTop:20}}></View>
        <View style={{flexDirection:'row',justifyContent:'space-between',marginRight:20,marginLeft:20,marginTop:20}}>
            <Text style={{fontWeight:'bold',fontSize:14}}>Sub Total:</Text>
            <Text style={{fontWeight:'bold',fontSize:14}}>£{parseInt(this.state.totalPrice)- parseInt(this.state.discount)}</Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-between',marginRight:20,marginLeft:20,marginTop:20}}>
            <Text style={{fontWeight:'bold',fontSize:14}}>Discount:</Text>
            <Text style={{fontWeight:'bold',fontSize:14}}>£{this.state.discount}</Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-between',marginRight:20,marginLeft:20,marginTop:20}}>
            <Text style={{fontWeight:'bold',fontSize:14}}>Discount Charges:</Text>
            <Text style={{fontWeight:'bold',fontSize:14}}>£{this.state.discountCharge}</Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-between',marginRight:20,marginLeft:20,marginTop:20}}>
            <Text style={{fontWeight:'bold',fontSize:14}}>Delivery Fee:</Text>
            <Text style={{fontWeight:'bold',fontSize:14}}>£ {this.state.deliveryCharge}</Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-between',marginRight:20,marginLeft:20,marginTop:20,backgroundColor:'#e4e4e4',padding:10}}>
            <Text style={{fontWeight:'bold',fontSize:14}}>Total Payment ({this.state.paymentMethod}):</Text>
            <Text style={{fontWeight:'bold',fontSize:14}}>£{this.state.totalPrice}</Text>
        </View>
        <View style={{alignItems:'center',marginRight:20,marginLeft:20,marginTop:20}}>
            <Text style={{fontWeight:'bold',fontSize:14}}>Delivered To: {this.state.name}</Text>
            <Text style={{fontWeight:'bold',fontSize:14}}>{this.state.userAddress}</Text>
            <Icon onPress={()=>{
                Linking.openURL(`tel:${this.state.userPhone}`)
            }} type="Entypo" name="phone" style={{fontSize:38,color:'green',marginTop:50}} />
        </View>
        
             </ScrollView>
       
          </View>
          
    
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    list: {
        marginTop:20,
        marginLeft:60,
        marginRight:50,
        marginBottom:-500
      },
      head: { height: 40, backgroundColor: '#fff'},
      text: { margin: 6,textAlign:'left',paddingLeft:20,fontWeight:'bold' }
   
})

const mapStateToProps = state => {
    return {
      userId: state.auth.userId,
      uri:state.auth.uri,
      activeBluetoothPrinter:state.auth.activeBluetoothPrinter,
      printerType:state.auth.printer
    }
  };

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Order)
