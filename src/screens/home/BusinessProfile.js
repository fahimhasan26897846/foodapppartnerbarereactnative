import React, { Component } from 'react'
import { View, Text,StyleSheet ,TextInput,Image, ScrollView,ToastAndroid} from 'react-native'
import { connect } from 'react-redux'
import {Container,Header,Tabs,Tab,Card,CardItem,Body,Left,Right,Icon,CheckBox} from "native-base"
import {Picker} from '@react-native-picker/picker';
import {Switch} from "react-native-elements"
import { RadioButton } from 'react-native-paper';


export class BusinessProfile extends Component {

    state = {
        name:'',
        email:'',
        mobileNumber:'',
        
    }

    render() {
        
        return (
            <View style={styles.container}>
                 <Tabs tabBarUnderlineStyle={{backgroundColor:'#d2181b'}}>
                 <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}}  tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white" }} heading="BUSINESS">
                      <Business baseUri={this.props.baseUri} uri={this.props.uri} userId={this.props.userId} navigation={this.props.navigation} />
                  </Tab>
                  <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}} tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white", }} heading="OWNER">
                      <Owner email={this.props.email} navigation={this.props.navigation} />
                  </Tab>
                  
                </Tabs>
            </View>
        )
    }
}

export class Owner extends Component {

    state = {
        email:'',
        name:'',
        mobileNumber:'',
        editable:false
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{width:'100%',height:50,flexDirection:'row',justifyContent:'space-between',padding:10}}>
                <Text style={{color:'#646464',fontWeight:'bold'}}>OWNER</Text>
                {this.state.editable? <Icon onPress={()=>{this.setState({editable:false})}} type="FontAwesome"  name="check" style={{color:'#646464'}}/>:<Icon onPress={()=>{this.setState({editable:true})}} type="FontAwesome"  name="edit" style={{color:'#646464'}}/>}
                                 
                </View>
                <Card style={{bolderBottomWidth:5,borderBottomColor:'#000'}}>
                    <CardItem>
                        <Body>
                        <TextInput onChangeText={value=>{this.setState({name:value})}} editable={this.state.editable} value={this.state.name} placeholder={'Name'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <TextInput placeholder={'Email'} editable={false} value={this.props.email} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <TextInput onChangeText={value=>{this.setState({mobileNumber:value})}} editable={this.state.editable} value={this.state.mobileNumber} placeholder={'Phone Number'} keyboardType={'numeric'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        </Body>
                    </CardItem>
                </Card>
            </View>
        )
    }
}

export class Business extends Component {

    state = {
        imgUri:'',
        time:'',
        name:'',
        title:'',
        speciality:'',
        location:'',
        editable:false,
        openingTime:'',
        closingTime:'',
        city:'',
        timeDelivery:'',
        postCode:'',
        serviceAvailable:true,
        openingTime:'12.30',
        closingTime:'',
        riderService:true,
        deliveryService:true,
        collectionService:true,
        fourty:false,
        sixty:false,
        ninety:false,
        freeDelivery:false,
        bookingService:false
        

    }

    componentDidMount = ()=>{
        fetch(this.props.uri+'admin/fetch-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
            console.log(responseJson.adminUser.restaurants);
            this.setState({title:responseJson.adminUser.restaurants[0].name,imgUri:this.props.baseUri+responseJson.adminUser.restaurants[0].logo,speciality:responseJson.adminUser.restaurants[0].speciality.toString(),location:responseJson.adminUser.restaurants[0].address,postCode:responseJson.adminUser.restaurants[0].postCode,city:responseJson.adminUser.restaurants[0].city,openingTime:responseJson.adminUser.restaurants[0].openingTime,closingTime:responseJson.adminUser.restaurants[0].closingTime});
        });
    }

    render() {
        let photo = '';
        if(this.state.imgUri === ''){
            photo = require('../../../assets/resturent.png');
        }else{
            photo = {uri:this.state.imgUri}
        }
        return (
            <View style={styles.container}>
                <View style={{width:'100%',height:50,flexDirection:'row',justifyContent:'space-between',padding:10}}>
                <Text style={{color:'#646464',fontWeight:'bold'}}>BUSINESS</Text>
                {this.state.editable? <Icon onPress={()=>{this.setState({editable:false});
            ToastAndroid.show("profile updated",ToastAndroid.SHORT,ToastAndroid.CENTER);
            }} type="FontAwesome"  name="check" style={{color:'#646464'}}/>:<Icon onPress={()=>{this.setState({editable:true})}} type="FontAwesome"  name="edit" style={{color:'#646464'}}/>}                  
                </View>
                <ScrollView>
                     <Card style={{bolderBottomWidth:5,borderBottomColor:'#000'}}>
                    <CardItem>
                        <Body>
                        <Image source={photo} style={{height:100,width:100,backgroundColor:'black',marginTop:20,alignSelf:'center'}} />
                        <TextInput onChangeText={value=>{this.setState({title:value})}} editable={this.state.editable} value={this.state.title} placeholder={'Name'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <TextInput onChangeText={value=>{this.setState({speciality:value})}} editable={this.state.editable} value={this.state.speciality} placeholder={'Name'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <TextInput onChangeText={value=>{this.setState({location:value})}} editable={this.state.editable} value={this.state.location} placeholder={'Name'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <TextInput onChangeText={value=>{this.setState({city:value})}} editable={this.state.editable} value={this.state.city} placeholder={'Name'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <TextInput onChangeText={value=>{this.setState({postCode:value})}} editable={this.state.editable} value={this.state.postCode} placeholder={'Name'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Servive Available</Text>
                            {this.state.editable?<Switch onValueChange={()=>{
                                if(this.state.serviceAvailable){
                                    this.setState({serviceAvailable:false})
                                }else{
                                    this.setState({serviceAvailable:true})
                                }
                            }}  value={this.state.serviceAvailable} color="red"></Switch>:<Switch color="red" value="true" disabled />}
                            
                        </View>
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Opening Time</Text>
                            {this.state.editable?<Picker
                style={{width:150}}
  selectedValue={this.state.openingTime}
  onValueChange={(itemValue, itemIndex) =>{
    this.setState({openingTime:itemValue});
  }}>
  <Picker.Item label="1.00AM" value="1.00AM" />
  <Picker.Item label="1.30AM" value="1.30AM" />
  <Picker.Item label="2.00AM" value="2.00AM" />
  <Picker.Item label="2.30AM" value="2.30AM" />
  <Picker.Item label="3.00AM" value="3.00AM" />
  <Picker.Item label="3.30AM" value="3.30AM" />
  <Picker.Item label="4.00AM" value="4.00AM" />
  <Picker.Item label="4.30" value="4.30AM" />
  <Picker.Item label="5.00AM" value="5.00AM" />
  <Picker.Item label="5.30AM" value="5.30AM" />
  <Picker.Item label="6.00AM" value="6.00AM" />
  <Picker.Item label="6.30AM" value="6.30AM" />
  <Picker.Item label="7.00AM" value="7.00AM" />
  <Picker.Item label="7.30AM" value="7.30AM" />
  <Picker.Item label="8.00AM" value="8.00AM" />
  <Picker.Item label="8.30AM" value="8.30AM" />
  <Picker.Item label="9.00AM" value="9.30AM" />
  <Picker.Item label="10.00AM" value="10.00AM" />
  <Picker.Item label="10.30AM" value="10.30AM" />
  <Picker.Item label="11.00AM" value="11.00AM" />
  <Picker.Item label="11.30AM" value="11.30AM" />
  <Picker.Item label="12.00AM" value="12.00AM" />
  <Picker.Item label="12.30" value="12.30AM" />

  <Picker.Item label="1.00PM" value="1.00PM" />
  <Picker.Item label="1.30PM" value="1.30PM" />
  <Picker.Item label="2.00PM" value="2.00PM" />
  <Picker.Item label="2.30PM" value="2.30PM" />
  <Picker.Item label="3.00PM" value="3.00PM" />
  <Picker.Item label="3.30PM" value="3.30PM" />
  <Picker.Item label="4.00PM" value="4.00PM" />
  <Picker.Item label="4.30PM" value="4.30PM" />
  <Picker.Item label="5.00PM" value="5.00PM" />
  <Picker.Item label="5.30PM" value="5.30PM" />
  <Picker.Item label="6.00PM" value="6.00PM" />
  <Picker.Item label="6.30PM" value="6.30PM" />
  <Picker.Item label="7.00PM" value="7.00PM" />
  <Picker.Item label="7.30PM" value="7.30PM" />
  <Picker.Item label="8.00PM" value="8.00PM" />
  <Picker.Item label="8.30PM" value="8.30PM" />
  <Picker.Item label="9.00PM" value="9.30PM" />
  <Picker.Item label="10.00PM" value="10.00PM" />
  <Picker.Item label="10.30PM" value="10.30PM" />
  <Picker.Item label="11.00PM" value="11.00PM" />
  <Picker.Item label="11.30PM" value="11.30PM" />
  <Picker.Item label="12.00PM" value="12.00PM" />
  <Picker.Item label="12.30PM" value="12.30PM" />
  
  

</Picker>:<Picker disabled={true}
                style={{width:150}}
  selectedValue={this.state.openingTime}
  onValueChange={(itemValue, itemIndex) =>{
    this.setState({selected:itemValue});
  }}>
  <Picker.Item label="1.00AM" value="1.00AM" />
  <Picker.Item label="1.30AM" value="1.30AM" />
  <Picker.Item label="2.00AM" value="2.00AM" />
  <Picker.Item label="2.30AM" value="2.30AM" />
  <Picker.Item label="3.00AM" value="3.00AM" />
  <Picker.Item label="3.30AM" value="3.30AM" />
  <Picker.Item label="4.00AM" value="4.00AM" />
  <Picker.Item label="4.30" value="4.30AM" />
  <Picker.Item label="5.00AM" value="5.00AM" />
  <Picker.Item label="5.30AM" value="5.30AM" />
  <Picker.Item label="6.00AM" value="6.00AM" />
  <Picker.Item label="6.30AM" value="6.30AM" />
  <Picker.Item label="7.00AM" value="7.00AM" />
  <Picker.Item label="7.30AM" value="7.30AM" />
  <Picker.Item label="8.00AM" value="8.00AM" />
  <Picker.Item label="8.30AM" value="8.30AM" />
  <Picker.Item label="9.00AM" value="9.30AM" />
  <Picker.Item label="10.00AM" value="10.00AM" />
  <Picker.Item label="10.30AM" value="10.30AM" />
  <Picker.Item label="11.00AM" value="11.00AM" />
  <Picker.Item label="11.30AM" value="11.30AM" />
  <Picker.Item label="12.00AM" value="12.00AM" />
  <Picker.Item label="12.30" value="12.30AM" />

  <Picker.Item label="1.00PM" value="1.00PM" />
  <Picker.Item label="1.30PM" value="1.30PM" />
  <Picker.Item label="2.00PM" value="2.00PM" />
  <Picker.Item label="2.30PM" value="2.30PM" />
  <Picker.Item label="3.00PM" value="3.00PM" />
  <Picker.Item label="3.30PM" value="3.30PM" />
  <Picker.Item label="4.00PM" value="4.00PM" />
  <Picker.Item label="4.30PM" value="4.30PM" />
  <Picker.Item label="5.00PM" value="5.00PM" />
  <Picker.Item label="5.30PM" value="5.30PM" />
  <Picker.Item label="6.00PM" value="6.00PM" />
  <Picker.Item label="6.30PM" value="6.30PM" />
  <Picker.Item label="7.00PM" value="7.00PM" />
  <Picker.Item label="7.30PM" value="7.30PM" />
  <Picker.Item label="8.00PM" value="8.00PM" />
  <Picker.Item label="8.30PM" value="8.30PM" />
  <Picker.Item label="9.00PM" value="9.30PM" />
  <Picker.Item label="10.00PM" value="10.00PM" />
  <Picker.Item label="10.30PM" value="10.30PM" />
  <Picker.Item label="11.00PM" value="11.00PM" />
  <Picker.Item label="11.30PM" value="11.30PM" />
  <Picker.Item label="12.00PM" value="12.00PM" />
  <Picker.Item label="12.30PM" value="12.30PM" />
  
  

</Picker>}
                            
                        </View>
                       
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Closing Time</Text>
                            {this.state.editable?<Picker
                style={{width:150}}
  selectedValue={this.state.closingTime}
  onValueChange={(itemValue, itemIndex) =>{
    this.setState({closingTime:itemValue});
  }}>
  <Picker.Item label="1.00AM" value="1.00AM" />
  <Picker.Item label="1.30AM" value="1.30AM" />
  <Picker.Item label="2.00AM" value="2.00AM" />
  <Picker.Item label="2.30AM" value="2.30AM" />
  <Picker.Item label="3.00AM" value="3.00AM" />
  <Picker.Item label="3.30AM" value="3.30AM" />
  <Picker.Item label="4.00AM" value="4.00AM" />
  <Picker.Item label="4.30" value="4.30AM" />
  <Picker.Item label="5.00AM" value="5.00AM" />
  <Picker.Item label="5.30AM" value="5.30AM" />
  <Picker.Item label="6.00AM" value="6.00AM" />
  <Picker.Item label="6.30AM" value="6.30AM" />
  <Picker.Item label="7.00AM" value="7.00AM" />
  <Picker.Item label="7.30AM" value="7.30AM" />
  <Picker.Item label="8.00AM" value="8.00AM" />
  <Picker.Item label="8.30AM" value="8.30AM" />
  <Picker.Item label="9.00AM" value="9.30AM" />
  <Picker.Item label="10.00AM" value="10.00AM" />
  <Picker.Item label="10.30AM" value="10.30AM" />
  <Picker.Item label="11.00AM" value="11.00AM" />
  <Picker.Item label="11.30AM" value="11.30AM" />
  <Picker.Item label="12.00AM" value="12.00AM" />
  <Picker.Item label="12.30" value="12.30AM" />

  <Picker.Item label="1.00PM" value="1.00PM" />
  <Picker.Item label="1.30PM" value="1.30PM" />
  <Picker.Item label="2.00PM" value="2.00PM" />
  <Picker.Item label="2.30PM" value="2.30PM" />
  <Picker.Item label="3.00PM" value="3.00PM" />
  <Picker.Item label="3.30PM" value="3.30PM" />
  <Picker.Item label="4.00PM" value="4.00PM" />
  <Picker.Item label="4.30PM" value="4.30PM" />
  <Picker.Item label="5.00PM" value="5.00PM" />
  <Picker.Item label="5.30PM" value="5.30PM" />
  <Picker.Item label="6.00PM" value="6.00PM" />
  <Picker.Item label="6.30PM" value="6.30PM" />
  <Picker.Item label="7.00PM" value="7.00PM" />
  <Picker.Item label="7.30PM" value="7.30PM" />
  <Picker.Item label="8.00PM" value="8.00PM" />
  <Picker.Item label="8.30PM" value="8.30PM" />
  <Picker.Item label="9.00PM" value="9.30PM" />
  <Picker.Item label="10.00PM" value="10.00PM" />
  <Picker.Item label="10.30PM" value="10.30PM" />
  <Picker.Item label="11.00PM" value="11.00PM" />
  <Picker.Item label="11.30PM" value="11.30PM" />
  <Picker.Item label="12.00PM" value="12.00PM" />
  <Picker.Item label="12.30PM" value="12.30PM" />
  
  

</Picker>:<Picker disabled={true}
                style={{width:150}}
  selectedValue={this.state.openingTime}
  onValueChange={(itemValue, itemIndex) =>{
    this.setState({selected:itemValue});
  }}>
  <Picker.Item label="1.00AM" value="1.00AM" />
  <Picker.Item label="1.30AM" value="1.30AM" />
  <Picker.Item label="2.00AM" value="2.00AM" />
  <Picker.Item label="2.30AM" value="2.30AM" />
  <Picker.Item label="3.00AM" value="3.00AM" />
  <Picker.Item label="3.30AM" value="3.30AM" />
  <Picker.Item label="4.00AM" value="4.00AM" />
  <Picker.Item label="4.30" value="4.30AM" />
  <Picker.Item label="5.00AM" value="5.00AM" />
  <Picker.Item label="5.30AM" value="5.30AM" />
  <Picker.Item label="6.00AM" value="6.00AM" />
  <Picker.Item label="6.30AM" value="6.30AM" />
  <Picker.Item label="7.00AM" value="7.00AM" />
  <Picker.Item label="7.30AM" value="7.30AM" />
  <Picker.Item label="8.00AM" value="8.00AM" />
  <Picker.Item label="8.30AM" value="8.30AM" />
  <Picker.Item label="9.00AM" value="9.30AM" />
  <Picker.Item label="10.00AM" value="10.00AM" />
  <Picker.Item label="10.30AM" value="10.30AM" />
  <Picker.Item label="11.00AM" value="11.00AM" />
  <Picker.Item label="11.30AM" value="11.30AM" />
  <Picker.Item label="12.00AM" value="12.00AM" />
  <Picker.Item label="12.30" value="12.30AM" />

  <Picker.Item label="1.00PM" value="1.00PM" />
  <Picker.Item label="1.30PM" value="1.30PM" />
  <Picker.Item label="2.00PM" value="2.00PM" />
  <Picker.Item label="2.30PM" value="2.30PM" />
  <Picker.Item label="3.00PM" value="3.00PM" />
  <Picker.Item label="3.30PM" value="3.30PM" />
  <Picker.Item label="4.00PM" value="4.00PM" />
  <Picker.Item label="4.30PM" value="4.30PM" />
  <Picker.Item label="5.00PM" value="5.00PM" />
  <Picker.Item label="5.30PM" value="5.30PM" />
  <Picker.Item label="6.00PM" value="6.00PM" />
  <Picker.Item label="6.30PM" value="6.30PM" />
  <Picker.Item label="7.00PM" value="7.00PM" />
  <Picker.Item label="7.30PM" value="7.30PM" />
  <Picker.Item label="8.00PM" value="8.00PM" />
  <Picker.Item label="8.30PM" value="8.30PM" />
  <Picker.Item label="9.00PM" value="9.30PM" />
  <Picker.Item label="10.00PM" value="10.00PM" />
  <Picker.Item label="10.30PM" value="10.30PM" />
  <Picker.Item label="11.00PM" value="11.00PM" />
  <Picker.Item label="11.30PM" value="11.30PM" />
  <Picker.Item label="12.00PM" value="12.00PM" />
  <Picker.Item label="12.30PM" value="12.30PM" />
  
  

</Picker>}
                            
                        </View>
                       
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Rider Service</Text>
                            {this.state.editable?<Switch onValueChange={()=>{
                                if(this.state.riderService){
                                    this.setState({riderService:false})
                                }else{
                                    this.setState({riderService:true})
                                }
                            }}  value={this.state.riderService} color="red"></Switch>:<Switch color="red" value="true" disabled />}
                            
                        </View>
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Delivery Service</Text>
                            {this.state.editable?<Switch onValueChange={()=>{
                                if(this.state.riderService){
                                    this.setState({deliveryService:false})
                                }else{
                                    this.setState({deliveryService:true})
                                }
                            }}  value={this.state.deliveryService} color="red"></Switch>:<Switch color="red" value="true" disabled />}
                            
                        </View>
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Collection Service</Text>
                            {this.state.editable?<Switch onValueChange={()=>{
                                if(this.state.riderService){
                                    this.setState({collectionService:false})
                                }else{
                                    this.setState({collectionService:true})
                                }
                            }}  value={this.state.collectionService} color="red"></Switch>:<Switch color="red" value="true" disabled />}
                            
                        </View>
                        <TextInput onChangeText={value=>{this.setState({postCode:value})}} editable={this.state.editable} placeholder={'Discount type'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <TextInput onChangeText={value=>{this.setState({postCode:value})}} editable={this.state.editable} placeholder={'Discount'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:10,width:'100%'}}>
                        <RadioButton.Group style={{flexDirection:'row',width:'100%'}} onValueChange={newValue => this.setState({timeDelivery:newValue})} value={this.state.timeDelivery} row>
      <View style={{flexDirection:'row'}}>
        <Text>40 min</Text>
        <RadioButton value="40min" />
      </View>
      <View style={{flexDirection:'row'}}>
        <Text>60 min</Text>
        <RadioButton value="60min" />
      </View>
      <View style={{flexDirection:'row'}}>
        <Text>90 min</Text>
        <RadioButton value="90min" />
      </View>
    </RadioButton.Group>
                        </View>
                        <TextInput onChangeText={value=>{this.setState({postCode:value})}} editable={this.state.editable} placeholder={'Minimum Order'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Free Delivery</Text>
                            {this.state.editable?<Switch onValueChange={()=>{
                                if(this.state.freeDelivery){
                                    this.setState({freeDelivery:false})
                                }else{
                                    this.setState({freeDelivery:true})
                                }
                            }}  value={this.state.freeDelivery} color="red"></Switch>:<Switch color="red" value="true" disabled />}
                            
                        </View>
                        <TextInput onChangeText={value=>{this.setState({postCode:value})}} editable={this.state.editable} placeholder={'Delivery Charges'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                        <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',padding:10}}>
                            <Text>Booking Service</Text>
                            {this.state.editable?<Switch onValueChange={()=>{
                                if(this.state.bookingService){
                                    this.setState({bookingService:false})
                                }else{
                                    this.setState({bookingService:true})
                                }
                            }}  value={this.state.bookingService} color="red"></Switch>:<Switch color="red" value="true" disabled />}
                            
                        </View>
                       
                        
                        </Body>
                    </CardItem>
                </Card>
         
                </ScrollView>
                  </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff'
    }
})

const mapStateToProps = state => {
    return {
      userId: state.auth.userId,
      uri:state.auth.uri,
      baseUri:state.auth.baseUri,
      email:state.auth.email
    }
  };

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(BusinessProfile)
