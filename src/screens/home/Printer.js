import React, { Component } from 'react'
import { View, Text,StyleSheet,ScrollView,Linking,ToastAndroid,FlatList } from 'react-native'
import { connect } from 'react-redux'
import {Header,Container,Left,Right,Body,Icon,Title,Button, Toast} from "native-base"
import {Picker} from '@react-native-picker/picker';
import {BLEPrinter} from "react-native-thermal-receipt-printer";
import BluetoothStateManager from 'react-native-bluetooth-state-manager';

import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component'
export class Printer extends Component {
    state = {
        activePrinter:'',
        selected:'bluetooth',
        printers:[],
        selectedPrinter:'',
        save:'Save',
        editing:false
    }
    savePrinterSettings = ()=>{
      this.props.changePrinter(this.state.selectedPrinter);
      this.props.changePrinterType(this.state.selected);
      ToastAndroid.show('Printer setup executed',ToastAndroid.SHORT,ToastAndroid.CENTER);
    }
    onValueChange(value){
        this.setState({
          selected: value
        });
      }
      printContent = async()=>{
        await BluetoothStateManager.enable().then((result) => {
             BLEPrinter.init().then(()=> {
                BLEPrinter.getDeviceList().then(value=>{
                  this.setState({printers:value});
                });
              });
        }); 
    }
    componentDidMount=()=>{
      this.printContent();
      this.setState({selected:this.props.printerType,selectedPrinter:this.props.activeBluetoothPrinter})
    }
    
    render() {
        return (
            <View style={{flex:1,backgroundColor:'#fff'}}>
                 <Header style={{ backgroundColor: 'white',}}
          androidStatusBarColor="white">
              <Left>
                <Button onPress={()=>{this.props.navigation.goBack()}} transparent>
                  <Icon style={{color:'black'}} name='arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title style={{color:'black'}}>Printer</Title>
              </Body>
              <Right>
                {this.state.editing?<Button onPress={()=>{this.savePrinterSettings();
                  this.setState({save:'Saved',editing:false})}} style={{backgroundColor:'#d2181b',height:30,paddingLeft:15,paddingRight:15}}>
                  <Text style={{fontSize:10,color:'white'}}>{this.state.save}</Text>
                </Button>:<Icon onPress={()=>{
                  this.setState({editing:true})
                }} type="FontAwesome"  name="edit" style={{color:'#646464'}}/>}
                
              </Right>
            </Header>
            <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between'}}>
                <Text style={{padding:15,fontSize:14,width:150}}>Connection Type</Text>
                {this.state.editing? <Picker
                style={{width:150}}
  selectedValue={this.state.selected}
  onValueChange={(itemValue, itemIndex) =>{
    this.setState({selected:itemValue});
  }}>
  <Picker.Item label="Bluetooth" value="bluetooth" />
  <Picker.Item label="USB" value="usb" />

</Picker>:<Text style={{padding:15,fontSize:14,width:150,textTransform:'capitalize'}}>{this.state.selected}</Text>}
               
            </View>
            <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between'}}>
                <Text style={{padding:15,fontSize:14,width:150}}>Printer Name</Text>
                {this.state.editing?<Picker
                style={{width:150}}
  selectedValue={this.state.selectedPrinter}
  onValueChange={(itemValue, itemIndex) =>{
    this.setState({selectedPrinter:itemValue});
  }}>
    
  {this.state.printers.map( (s, i) => {
            return <Picker.Item key={i} value={s.inner_mac_address} label={s.device_name} />
        })}
</Picker>: <Text style={{padding:15,fontSize:14,width:150,textTransform:'capitalize'}}>{this.state.selectedPrinter}</Text>}
               
                
            </View>
           
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
      host: state.auth.host,
      activeBluetoothPrinter:state.auth.activeBluetoothPrinter,
      printerType:state.auth.printerType
      
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return{
        changeAccessToken : (value) => {dispatch({type:'CHANGE_TOKEN',token: value})},
        changeLogged : (value) => {dispatch({type:'LOGIN',logged: value})},
        changeInfo:(value)=>{dispatch({type:'PROFILE_SET',info:value})},
        changePrinter:(value)=>{dispatch({type:'SET_PRINTER',printer:value})},
        changePrinterType:(value)=>{dispatch({type:'SET_PRINTER_TYPE',printer:value})},
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Printer)
