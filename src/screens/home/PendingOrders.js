import React, { Component } from 'react'
import { View, Text,FlatList,TouchableOpacity,ToastAndroid } from 'react-native'
import {Card,CardItem,Body,Icon,Badge} from "native-base"
import {BLEPrinter} from "react-native-thermal-receipt-printer";
import BluetoothStateManager from 'react-native-bluetooth-state-manager';

import { connect } from 'react-redux'

export class PendingOrders extends Component {
    state= {
        orders:[],
        resturentName:'',
        resturentLocation:'',
        orderNumber:'',
        name:'',
        userAddress:'',
        userPhone:'',
        orderDate:'',
        totalPrice:'',
        printing:true,
        printers:[],
        currentPrinter:'',
        foodItems:[],
        paymentMethod:'',
        deliveryCharge:'',
        discountCharge:'',
        discount:'',
    }

    printBillTest = () => {
        var dates = new Date(this.state.orderDate);
        this.state.currentPrinter && BLEPrinter.printBill("<CM>"+this.state.resturentName+"</CM>\n<C>Address:"+this.state.resturentLocation+"</C>\n<C>Date:"+dates.getDate().toString()+"/"+dates.getMonth().toString()+"/"+dates.getFullYear().toString()+"</C>\n\n<C>Order no:"+this.state.orderNumber+"</C>\n<C>"+"QTY"+" "+" "+" "+" "+"Description"+" "+" "+" "+" "+"Amount</C>\n____________________________\n\n"+this.state.foodItems.map(arr=>"<C>"+arr.quantity.toString()+" "+" "+" "+" "+arr.title.toString()+" "+" "+" "+" "+" "+arr.price.toString()+"</C>\n")+"\n\n____________________________\n\n<C>GROSS TOTAL:"+" "+" "+" "+" "+" "+" "+this.state.totalPrice+"</C>\n<C>DISCOUNT CHARGES:"+" "+" "+" "+"" /+" "+" "+this.state.deliveryCharge+"  </C>\n<C>DELIVERY FEES:"+" "+" "+" "+" "+" "+this.state.deliveryCharge+"  </C>\n<C>SAVINGS</C>\n________________________________\n<L>  TOTAL PAYMENT (CASH/CARD)  </L><R>  "+this.state.totalPrice.toString()+"  </R>\n\n\n\n\n\n<C>DELIVERY TO: "+this.state.userName+", "+this.state.userAddress+"</C>\n\n<C>OR,</C>\n\n\n<C>COLLECTION FOR CUSTOMER NAME</C>");
      }

    _connectPrinter =(printer) => {
        BLEPrinter.connectPrinter(printer).then(value=>{
          this.setState({currentPrinter:value});
        })
        .then(()=>{
          this.printBillTest();
        }).then(()=>{
          this.setState({printing:false});
          ToastAndroid.show('Printing completed',ToastAndroid.CENTER,ToastAndroid.SHORT);
        })
      }

    printContent = async()=>{
        await BluetoothStateManager.enable().then((result) => {
             BLEPrinter.init().then(()=> {
                BLEPrinter.getDeviceList().then(value=>{
                  if(this.props.activeBluetoothPrinter === ''){
                    ToastAndroid.show('Set your printer first',ToastAndroid.CENTER,ToastAndroid.SHORT);
                   
                  }else{
                     this._connectPrinter(this.props.activeBluetoothPrinter);
                  }
                  
                });
              });
        });
      
    }

    removeOrder = (id)=>{
        // fetch(this.props.uri+'order/remove/'+id).then(()=>{
        //     fetch(this.props.uri+'order/fetch-by-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
                
        //         var newArray = responseJson.filter((item)=>{
        //             return item.orderStatus == "recieved";         
        //         })
        //         this.setState({orders:newArray});
        //         })
        // })
    }

    acceptOrder = (id,values) =>{
       
        var newArray = [];
        for(var i=0;i<values.foodItems.length;i++){
            var modifiedArray = [values.foodItems[i].quantity,values.foodItems[i].title,'£ '+parseInt(values.foodItems[i].price)*parseInt(values.foodItems[i].quantity)];
        newArray.push(modifiedArray);}
        this.setState({resturentName:values.restaurant.name,orderNumber:values.orderNumber,resturentLocation:values.address,orderDate:values.createdAt,totalPrice:values.totalPrice,paymentMethod:values.paymentMethod,deliveryCharge:values.deliveryCharge,discount:values.discount,discountCharge:values.restaurant.deliveryCharges,name:values.user.name,userAddress:values.user.address+','+values.user.postCode,userPhone:values.user.mobile,tableData:newArray,foodItems:values.foodItems});
        fetch(this.props.uri+'order/accept-order-by-owner',{
                method: 'POST',
                body:JSON.stringify({
                    orderId:id
                })
        }).then(()=>{            
            fetch(this.props.uri+'order/fetch-by-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
                var newArrays = responseJson.filter((item)=>{
                    return item.orderStatus == "pending";         
                })
                this.setState({orders:newArrays});
                })
        })
    }

    declineOrder = (id) =>{
        ToastAndroid.show(id.toString(),ToastAndroid.SHORT,ToastAndroid.CENTER);
        fetch(this.props.uri+'order/decline-order-by-owner',{
            method: 'POST',
            body:JSON.stringify({
                orderId:id
            })
    }).then(()=>{
        fetch(this.props.uri+'order/fetch-by-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
            
            var newArray = responseJson.filter((item)=>{
                return item.orderStatus == "pending";         
            })
            this.setState({orders:newArray});
            })
    })
    }

    fetchTimeDate=(timestamp)=>{
        var iteim = new Date(timestamp).getDate();
        return iteim;
      }
    
      fetchTimeMonth=(timestamp)=>{
        var iteim = new Date(timestamp).getMonth();
        return iteim;
      }
    
      fetchTimeYear=(timestamp)=>{
        var iteim = new Date(timestamp).getYear();
        return iteim;
      }
    componentDidMount = async()=>{
        fetch(this.props.uri+'order/fetch-by-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
        console.log(responseJson);
        var newArray = responseJson.filter((item)=>{
            return item.orderStatus == "pending";         
        })
        this.setState({orders:newArray});
        })
    }
    render() {
        return (
            <View style={{flex:1,backgroundColor:'#ffffff'}}>
                {this.state.orders.length<1?<Card>
                  <CardItem>
                      <Body>
                          <Text style={{color:'#646464',fontSize:15}}>There is no order request</Text>
                      </Body>
                  </CardItem>
              </Card>:<FlatList
        data={this.state.orders}
        renderItem={({item})=><Card>
        <CardItem>
            <Body>   
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>{item.orderNumber}</Text>
                    
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>Order Date</Text>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>{this.fetchTimeDate(item.createdAt)+'/'+this.fetchTimeMonth(item.createdAt)+'/'+this.fetchTimeYear(item.createdAt)}</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>Delivery Time</Text>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>{item.deliveryTime}</Text>
                </View>
               
                <View style={{width:'100%',height:2,backgroundColor:'#646464',marginTop:15,marginBottom:15}}></View>
                <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingTop:15}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>Total Price</Text>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'#646464'}}>£{item.totalPrice}</Text>
                </View>
                <View style={{width:'100%',flexDirection:'row',justifyContent:'space-around',padding:5}}>
                    <TouchableOpacity onPress={()=>{
                        this.declineOrder(item._id);
                    }} style={{padding:10,backgroundColor:'black',margin:10,elevation:10}}><Text style={{color:'white'}}>DECLINE</Text></TouchableOpacity>
                    <TouchableOpacity  onPress={()=>{
                        this.acceptOrder(item._id,item);
                    }} style={{padding:10,backgroundColor:'#d2181b',margin:10,elevation:10}}><Text style={{color:'white'}}>ACCEPT</Text></TouchableOpacity>
                </View>
            </Body>
        </CardItem>
    </Card>}
        keyExtractor={item => item.id}
      />}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
      userId: state.auth.userId,
      uri:state.auth.uri,
      activeBluetoothPrinter:state.auth.activeBluetoothPrinter,
      printerType:state.auth.printer
    }
  };

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(PendingOrders)
