import React, { Component } from 'react'
import { View, Text,StatusBar,StyleSheet,Image,ActivityIndicator} from 'react-native'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage';

export class Loading extends Component {
    state = {
        id:'',
        email:''
    }

    componentDidMount = async()=>{
        const value = await AsyncStorage.getItem('loggedIn').then(values=>{
            if(values == null){
              AsyncStorage.setItem('loggedIn','false');
              setTimeout(() => {
               this.props.navigation.navigate('Login');
              }, 2000);
            }else{
            if(values === 'true'){
                const id = AsyncStorage.getItem('id').then(newId=>{
                    
                    this.setState({id:newId})
                });
                const printerd = AsyncStorage.getItem('activeBluetoothPrinter').then(printer=>{
                    if(printer !== null){
                        this.props.changePrinter(printer);
                    }
                });
                const printerType = AsyncStorage.getItem('printerType').then(printeri=>{
                    if(printeri !== null){
                        this.props.changePrinterType(printeri);
                    }
                })
                const email = AsyncStorage.getItem('email').then(newMail=>{
                   this.setState({email:newMail});
                })
                setTimeout(()=>{
                    this.props.changeInfo({
                        id:this.state.id,
                        email:this.state.email
                    });
                    this.props.changeLogged(true);
                },2000);
                
                
            }else if(values === 'false'){
                this.props.changeLogged(false);
                this.props.navigation.navigate('Login');
            }
            }
          });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" backgroundColor="transparent" translucent={true} />
                <Image source={require('../../../assets/icontr.png')} style={{height:200,width:200}} />
                <ActivityIndicator size="large" color={'#fff'} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#d2181b'
    }
})

const mapStateToProps = state => {
    return {
      host: state.auth.host,
    }
  };
  
const mapDispatchToProps = dispatch => {
    return{
        changeAccessToken : (value) => {dispatch({type:'CHANGE_TOKEN',token: value})},
        changeLogged : (value) => {dispatch({type:'LOGIN',logged: value})},
        changeInfo:(value)=>{dispatch({type:'PROFILE_SET',info:value})},
        changePrinter:(value)=>{dispatch({type:'SET_PRINTER',printer:value})},
        changePrinterType:(value)=>{dispatch({type:'SET_PRINTER_TYPE',printer:value})}
    };
};
  

export default connect(mapStateToProps, mapDispatchToProps)(Loading)
