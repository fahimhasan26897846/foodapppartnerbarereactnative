import AsyncStorage from '@react-native-async-storage/async-storage';

const data = {
    'host':'https://api.foodapps.uk/api/admin/',
    'uri':'https://api.foodapps.uk/api/',
    'baseUri':'https://api.foodapps.uk/',
    'email':'',
    'firstName':'',
    'printerType':'bluetooth',
    'activeBluetoothPrinter':'',
    'lastName':'',
    'accessToken':'',
    'profilePicture':'',
    'userId':null,
    'phoneNumber':'',
    'loggedIn':false,
};

const reducer = (state = data, action) => {
    switch (action.type) {
        case 'LOGOUT':
            AsyncStorage.multiSet([['loggedIn', 'false'],['id', ''],['token', ''],['email', ''],['first_name', ''],['last_name', ''],['phone_number', ''],['profile_picture', '']])
            return {
                ...state,
                loggedIn: action.logged,
            };
        case 'LOGIN':
            return {
                ...state,
                loggedIn: action.logged
            };
        case 'CHANGE_TOKEN':
            return {
                ...state,
                accessToken: action.token
            };
        case 'CHANGE_DP':
                    return {
                        ...state,
                        profilePicture: action.profile_picture,
                                    
                    };
        case 'CHANGE_BASIC_INFO':
            return {
                ...state,
                firstName:action.info.firstName,
                lastName:action.info.lastName,
                phoneNumber:action.info.phoneNumber
            }
        case 'PROFILE_SET':
            return {
                    ...state,
                    userId:action.info.id,
                    email:action.info.email,
                }
        case 'SET_PRINTER':
            AsyncStorage.setItem('activeBluetoothPrinter',action.printer);
            return {
                ...state,
                activeBluetoothPrinter:action.printer
            }
        case 'SET_PRINTER_TYPE': 
            AsyncStorage.setItem('printerType',action.printer);
            return {
                ...state,
                printerType:action.printer
                }
        
        default:
            return state;
    }
};


export default reducer;


