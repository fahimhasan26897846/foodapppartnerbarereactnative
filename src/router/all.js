import Home from "../screens/home/Home"

import Login from "../screens/auth/Login"
import Loading from "../screens/auth/Loading"
import MyBookings from "../screens/home/MyBookings"
import Orders from "../screens/home/Orders"
import BusinessProfile from "../screens/home/BusinessProfile"
import Printer from "../screens/home/Printer"
import SalesReport from "../screens/home/SalesReport"
import HouseOrder from "../screens/home/HouseOrder"
import Order from "../screens/home/Order"
import Register from '../screens/auth/Register'
import ForgotPassword from "../screens/auth/ForgotPassword"
import ResetPassword from "../screens/auth/ResetPassword"
import Chat from "../screens/home/Chat"
import Settings from "../screens/home/Settings"
import Driver from "../screens/home/Driver"
import Notifications from "../screens/home/Notifications"
import Termsofuse from "../screens/home/Termsofuse"


export {Home,Login,Loading,Register,Driver,SalesReport,MyBookings,BusinessProfile,HouseOrder,Printer,Order,Orders,ForgotPassword,ResetPassword,Chat,Settings,Termsofuse,Notifications}