import React, { Component } from 'react'
import { connect } from 'react-redux'
import {Home,Login,Register,Chat,Loading,Driver,MyBookings,BusinessProfile,Order,Settings,Printer,ForgotPassword,ResetPassword,Notifications,SalesReport,Termsofuse, Orders,HouseOrder} from "./all"
import {NavigationContainer} from "@react-navigation/native"
import {createStackNavigator,CardStyleInterpolators} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
const Tab = createMaterialBottomTabNavigator();
import {Icon} from "native-base"

function SecondStack(){
  return (<Stack.Navigator>
              <Stack.Screen name="MainScreen" options={defaultStackConfig} component={Home} />
              <Stack.Screen name="HouseOrder" options={{gestureEnabled:true,
  gestureDirection:'horizontal',title:'House Order', headerLeft:null ,cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }} component={HouseOrder} />
    </Stack.Navigator>)
}

function HomeTabs() {
  return (
    <Tab.Navigator  
    activeColor="#d2181b"
    inactiveColor="#adadad"
    barStyle={{ backgroundColor: '#ffffff',paddingBottom: 8,height:70 }}>
      <Tab.Screen name="Home"
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({ color }) => (
            <Icon type="Ionicons" name="home-outline" style={{fontSize:24,color:color}} />
        ),
      }}
      component={SecondStack} />
      <Tab.Screen name="Chat"
      options={{
        tabBarLabel: 'Chat',
        tabBarIcon: ({ color }) => (
            <Icon type="Ionicons" name="chatbubble-outline" style={{fontSize:24,color:color}}  />
        ),
      }}
      component={Chat} />
      <Tab.Screen name="Settings"
      options={{
        tabBarLabel: 'Settings',
        tabBarIcon: ({ color }) => (
            <Icon type="AntDesign" name="setting" style={{fontSize:24,color:color}} />
        ),
      }}
      component={Settings} />
    </Tab.Navigator>
  );
}
const Stack = createStackNavigator();
const defaultStackConfig = {
    headerShown: false,
    gestureEnabled:true,
    gestureDirection:'horizontal',
    cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS
}

export class Index extends Component {
    render() {
      if(!this.props.loggedIn){
        return (
          <NavigationContainer>
              <Stack.Navigator>
              <Stack.Screen name="Loading" options={defaultStackConfig} component={Loading} />
              <Stack.Screen name="Login" options={defaultStackConfig} component={Login} />
              <Stack.Screen name="Register" options={defaultStackConfig} component={Register} />
              <Stack.Screen name="ForgotPassword" options={defaultStackConfig} component={ForgotPassword} />
              <Stack.Screen name="ResetPassword" options={defaultStackConfig} component={ResetPassword} />
              </Stack.Navigator>
          </NavigationContainer>
      )
      }else{
        return (
          <NavigationContainer>
              <Stack.Navigator>
              <Stack.Screen name="HomeScreen" options={defaultStackConfig} children={HomeTabs} />
              <Stack.Screen name="Orders" options={{ title: 'Orders',gestureEnabled:true,
  gestureDirection:'horizontal',
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }} component={Orders} />
  <Stack.Screen name="Order" options={{ title: 'Orders',gestureEnabled:true,
  gestureDirection:'horizontal',headerShown: false,
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }} component={Order} />
  <Stack.Screen name="Printer" options={{ title: 'Orders',gestureEnabled:true,
  gestureDirection:'horizontal',headerShown: false,
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }} component={Printer} />
  <Stack.Screen name="Notifications" options={{gestureEnabled:true,
  gestureDirection:'horizontal',
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }}   component={Notifications} />
  <Stack.Screen name="Driver" options={{gestureEnabled:true,
  gestureDirection:'horizontal',title:'Driver Nearby',  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }}   component={Driver} />
  
  <Stack.Screen name="Termsofuse" options={{ title: 'Terms of use',gestureEnabled:true,
  gestureDirection:'horizontal',
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }}  component={Termsofuse} />
  <Stack.Screen name="BusinessProfile" options={{ title: 'Business Profile',gestureEnabled:true,
  gestureDirection:'horizontal',
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }}  component={BusinessProfile} />
  <Stack.Screen name="MyBookings" options={{ title: 'My Bookings',gestureEnabled:true,
  gestureDirection:'horizontal',
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }}  component={MyBookings} />
  <Stack.Screen name="SalesReport" options={{ title: 'Sales Report',gestureEnabled:true,
  gestureDirection:'horizontal',
  cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS }}  component={SalesReport} />
              </Stack.Navigator>
          </NavigationContainer>
      )
      }
      
    }
}


const mapStateToProps = state => {
  return {
    host: state.auth.host,
    loggedIn:state.auth.loggedIn
  }
};

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)
